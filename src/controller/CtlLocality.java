package controller;

import model.Locality;
import org.w3c.dom.Element;

public class CtlLocality extends CtlDom {
    private static final String ET_NAME = "name";
    private static final String ET_COUNTRY = "country";

    public static Locality read(Element elemLocality) {
        String name = getValorEtiqueta(ET_NAME, elemLocality);
        String country = getValorEtiqueta(ET_COUNTRY, elemLocality);

        return new Locality(name, country);
    }
}
