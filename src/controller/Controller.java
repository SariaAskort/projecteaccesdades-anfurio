package controller;

import exceptions.NotAFolderException;
import model.Locality;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class Controller {
    private ArrayList<Locality> localities;
    private CtlFile ctlFile;
    private File dataFolder = new File("data");
    private Document document;

    public Controller() {
        localities = new ArrayList<>();
    }

    public Controller(File dataFolder) {
        this.dataFolder = dataFolder;
        localities = new ArrayList<>();
    }

    public void read() throws ParserConfigurationException, SAXException, IOException, NotAFolderException {
        if (!dataFolder.exists()) throw new FileNotFoundException("The data folder doesn't exist.");
        if (!dataFolder.isDirectory()) throw new NotAFolderException("The file provided isn't a folder.");
        if (dataFolder.listFiles() == null) throw new NullPointerException("There's no files in the data folder.");
        for (File xml :
                dataFolder.listFiles()) {
            Document doc = CtlFile.recuperar(xml);
            localities.add(CtlFile.read(doc));
        }
    }

    public void showLocalities() {
        for (Locality locality :
                localities) {
            System.out.println(locality);
        }
    }

    public ArrayList<Locality> getLocalities() {
        return localities;
    }

    public void setLocalities(ArrayList<Locality> localities) {
        this.localities = localities;
    }

    public CtlFile getCtlFile() {
        return ctlFile;
    }

    public void setCtlFile(CtlFile ctlFile) {
        this.ctlFile = ctlFile;
    }

    public File getDataFolder() {
        return dataFolder;
    }

    public void setDataFolder(File dataFolder) {
        this.dataFolder = dataFolder;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
}
