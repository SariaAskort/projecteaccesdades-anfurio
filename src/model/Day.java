package model;

import java.time.LocalDate;
import java.time.LocalTime;

public class Day {
    private LocalDate date;
    private float temperature_max;
    private float temperature_min;
    private String text;
    private int humidity;
    private int wind;
    private String wind_direction;
    private LocalTime sunrise;
    private LocalTime sunset;
    private LocalTime moonrise;
    private LocalTime moonset;

    public Day(LocalDate date, float temperature_max, float temperature_min, String text, int humidity, int wind, String wind_direction, LocalTime sunrise, LocalTime sunset, LocalTime moonrise, LocalTime moonset) {
        this.date = date;
        this.temperature_max = temperature_max;
        this.temperature_min = temperature_min;
        this.text = text;
        this.humidity = humidity;
        this.wind = wind;
        this.wind_direction = wind_direction;
        this.sunrise = sunrise;
        this.sunset = sunset;
        this.moonrise = moonrise;
        this.moonset = moonset;
    }

    public Day() {
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public float getTemperature_max() {
        return temperature_max;
    }

    public void setTemperature_max(float temperature_max) {
        this.temperature_max = temperature_max;
    }

    public float getTemperature_min() {
        return temperature_min;
    }

    public void setTemperature_min(float temperature_min) {
        this.temperature_min = temperature_min;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getWind() {
        return wind;
    }

    public void setWind(int wind) {
        this.wind = wind;
    }

    public String getWind_direction() {
        return wind_direction;
    }

    public void setWind_direction(String wind_direction) {
        this.wind_direction = wind_direction;
    }

    public LocalTime getSunrise() {
        return sunrise;
    }

    public void setSunrise(LocalTime sunrise) {
        this.sunrise = sunrise;
    }

    public LocalTime getSunset() {
        return sunset;
    }

    public void setSunset(LocalTime sunset) {
        this.sunset = sunset;
    }

    public LocalTime getMoonrise() {
        return moonrise;
    }

    public void setMoonrise(LocalTime moonrise) {
        this.moonrise = moonrise;
    }

    public LocalTime getMoonset() {
        return moonset;
    }

    public void setMoonset(LocalTime moonset) {
        this.moonset = moonset;
    }
}
