package model;

import java.util.ArrayList;
import java.util.UUID;

public class Locality {
    private UUID id;
    private String name;
    private String country;
    private Information information;
    private ArrayList<Day> days;

    public Locality(String name, String country, Information information) {
        id = UUID.randomUUID();
        this.name = name;
        this.country = country;
        this.information = information;
        days = new ArrayList<>();
    }

    public Locality(String name, String country, Information information, ArrayList<Day> days) {
        id = UUID.randomUUID();
        this.name = name;
        this.country = country;
        this.information = information;
        this.days = days;
    }

    public Locality(String name, String country) {
        id = UUID.randomUUID();
        this.name = name;
        this.country = country;
    }

    public Locality() {
        id = UUID.randomUUID();
    }

    @Override
    public String toString() {
        return "Locality:\n\tName: " + this.name + "\n\tCountry: " + this.country + "\n\tDays: " + this.showDays();
    }

    private String showDays() {
        String result = "";
        for (Day day :
                this.days) {
            result += "\n\tDay:\n\t\tDate: " + day.getDate() + "\n\t\tMax temperature: " + day.getTemperature_max() + " " + information.getTemperature() +
                    "\n\t\tMin temperature: " + day.getTemperature_min() + " " + information.getTemperature() +
                    "\n\t\tPrevision: " + day.getText() + "\n\t\tHumidity: " + day.getHumidity() + " " + information.getHumidity() +
                    "\n\t\tWind: " + day.getWind() + " " + information.getWind() + "\n\t\tWind direction: " + day.getWind_direction() +
                    "\n\t\tSunrise: " + day.getSunrise() + "\n\t\tSunset: " + day.getSunset() + "\n\t\tMoonrise: " + day.getMoonrise() +
                    "\n\t\tMoonset: " + day.getMoonset();
        }

        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Information getInformation() {
        return information;
    }

    public void setInformation(Information information) {
        this.information = information;
    }

    public ArrayList<Day> getDays() {
        return days;
    }

    public void setDays(ArrayList<Day> days) {
        this.days = days;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
