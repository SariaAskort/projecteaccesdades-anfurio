package exceptions;

public class NotAFolderException extends Exception {
    public NotAFolderException(String message) {
        super(message);
    }
}
