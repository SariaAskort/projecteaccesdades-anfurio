package view;

import controller.Controller;
import exceptions.NotAFolderException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Controller controller = new Controller();
        int option = 0;
        boolean exit = false;

        do {
            exit = false;
            do {
                try {
                    menu();
                    option = scanner.nextInt();
                    exit = true;
                } catch (Exception e) {
                    System.out.println("You have to enter a number.");
                    scanner.nextLine();
                }
            } while (!exit);
            switch (option) {
                case 1:
                    try {
                        System.out.println("Reading data.");
                        controller.read();
                        System.out.println("Data read.");
                    } catch (ParserConfigurationException e) {
                        e.printStackTrace();
                    } catch (SAXException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NotAFolderException e) {
                        e.printStackTrace();
                    }
                    break;

                case 2:
                    controller.showLocalities();
                    break;

                case 3:
                    scanner.nextLine();
                    System.out.println("Enter new data location: ");
                    String location = scanner.nextLine();
                    controller = new Controller(new File(location));
                    System.out.println("Data folder location changed.");
                    break;

                case 0:
                    System.out.println("Exiting...");
                    break;

                default:
                    System.out.println("Option not recognized.");
                    break;
            }

        } while (option != 0);

    }

    private static void menu() {
        System.out.println("1.- Read from XML\n" +
                "2.- Show data\n" +
                "3.- Change data folder\n" +
                "0.- Exit");
        System.out.print("Select option: ");
    }
}
